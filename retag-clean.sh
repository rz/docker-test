#!/bin/sh
REG='https://registry.git-ce.rwth-aachen.de'
AUTH_BASE='https://git-ce.rwth-aachen.de/jwt/auth'
# www-authenticate: Bearer realm="https://git-ce.rwth-aachen.de/jwt/auth",service="container_registry",scope="repository:rz/docker-test/mampf:pull"
IMAGE_NAME='rz/docker-test/mampf'
USER=YOUR_USER_NAME
PASS=YOUR_PASSWORD

TAG_SOURCE="dev0"
TAG_TARGET="even-more-dev-ish"

AUTH_URL="$AUTH_BASE?service=container_registry&scope=repository:$IMAGE_NAME:pull,push"
MANIFEST_SOURCE="$REG/v2/$IMAGE_NAME/manifests/$TAG_SOURCE"
MANIFEST_TARGET="$REG/v2/$IMAGE_NAME/manifests/$TAG_TARGET"

MANIFEST_TYPE='application/vnd.docker.distribution.manifest.v2+json'

set -e

# auth
token=$(curl -s --user "$USER:$PASS" "$AUTH_URL" | jq -r .token)
authHeader="Authorization: Bearer $token"

# fetch source manifest
manifest=$(curl -s --fail-with-body -H "$authHeader" -H "Accept: $MANIFEST_TYPE" "$MANIFEST_SOURCE")

echo "manifest:"
echo "$manifest"

# PUT to new tag
curl -s --fail-with-body -X PUT -H "$authHeader" -H "Content-Type: $MANIFEST_TYPE" --data-binary "$manifest" "$MANIFEST_TARGET"

echo "done"

